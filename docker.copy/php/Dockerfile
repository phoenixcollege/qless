FROM pc_base_image

ARG XDEBUG_VERSION=2.6.1
ARG UBUNTU_VERSION=18.04
ARG PHPDOTVER=7.2
ARG NODE_INSTALL="node npm"
ARG WWW_USER=www-data
ARG WWW_GROUP=www-data
ARG USER_ID=1000
ARG GROUP_ID=1000

RUN \
  export DEBIAN_FRONTEND=noninteractive && \
  apt-get install -y php-cli php-fpm php-mysql php-curl php-gd \
  php-dev php-ldap php-xml php-bcmath php-mbstring \
  php-pear libsqlite3-dev unixodbc-dev mysql-client \
  build-essential libpcre3-dev ${NODE_INSTALL} && \
  test -z "$NODE_INSTALL" || ln -s /usr/bin/nodejs /usr/bin/node && \
  ACCEPT_EULA=Y apt-get install -y mssql-tools

RUN \
  echo 'Installing SQL Server libraries' && \
  pecl install sqlsrv && \
  pecl install pdo_sqlsrv

RUN \
  echo "Installing Xdebug version $XDEBUG_VERSION" && \
  wget http://xdebug.org/files/xdebug-${XDEBUG_VERSION}.tgz -O xdebug.tgz && \
  tar xzf xdebug.tgz && \
  rm -f xdebug.tgz && \
  cd xdebug-*/ && \
  phpize && \
  ./configure --with-php-config=/usr/bin/php-config && \
  make && \
  export TEST_PHP_ARGS='-n' && \
  make test && \
  make install && \
  cd .. && \
  rm -Rf xdebug-*/ && \
  apt-get clean && \
  apt-get autoremove -y && \
  rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Fix php session dir and error log
RUN \
  mkdir -p /var/lib/php/sessions && \
  chown -R ${WWW_USER}:${WWW_GROUP} /var/lib/php/sessions && \
  chmod 0777 -R /var/lib/php/sessions/ && \
  mkdir -p /var/log/php/ && \
  touch /var/log/php/error.log && \
  chmod 0755 /var/log/php/error.log && \
  chown -R ${WWW_USER}:${WWW_GROUP} /var/log/php/ && \
  chmod 0777 -R /var/log/php/

# PHP module configs
ADD conf/mods-available/xdebug.ini /etc/php/${PHPDOTVER}/mods-available/xdebug.ini
ADD conf/mods-available/mailcatcher.ini /etc/php/${PHPDOTVER}/mods-available/mailcatcher.ini
ADD conf/mods-available/sqlsrv.ini /etc/php/${PHPDOTVER}/mods-available/sqlsrv.ini
ADD conf/mods-available/pdo_sqlsrv.ini /etc/php/${PHPDOTVER}/mods-available/pdo_sqlsrv.ini

RUN \
  phpenmod xdebug && \
  phpenmod mailcatcher && \
  phpenmod sqlsrv && \
  phpenmod pdo_sqlsrv

# Create php pid location
RUN \
    mkdir -p /run/php

RUN \
  sed -i "s/;date.timezone =.*/date.timezone = America\/Phoenix/" /etc/php/${PHPDOTVER}/fpm/php.ini && \
  sed -i "s/;date.timezone =.*/date.timezone = America\/Phoenix/" /etc/php/${PHPDOTVER}/cli/php.ini && \
  sed -i "s/;cgi.fix_pathinfo=1/cgi.fix_pathinfo=0/" /etc/php/${PHPDOTVER}/fpm/php.ini && \
  sed -i "s/;cgi.fix_pathinfo=1/cgi.fix_pathinfo=0/" /etc/php/${PHPDOTVER}/cli/php.ini && \
  sed -i -e "s/;daemonize\s*=\s*yes/daemonize = no/g" /etc/php/${PHPDOTVER}/fpm/php-fpm.conf && \
  sed -ri 's/^TLS_CACERT/#TLS_CACERT/g' /etc/ldap/ldap.conf && \
  echo 'TLS_REQCERT never' >> /etc/ldap/ldap.conf

# Allow su www-data
ADD conf/dot-bash-rc /home/www-data/.bashrc
RUN \
   chsh -s /bin/bash ${WWW_USER} && \
   groupmod -g ${GROUP_ID} ${WWW_GROUP} && \
   usermod -u ${USER_ID} ${WWW_USER} && \
   chown -vR ${USER_ID}:${GROUP_ID} /home/${WWW_USER} && \
   usermod -d /home/${WWW_USER}/ ${WWW_USER} && \
   mkdir -p /app && \
   chown -R ${USER_ID}:${GROUP_ID} /app && \
   chmod -R 6775 /app


# Runit/init
ADD runit/php /etc/service/php/run
RUN chmod +x /etc/service/php/run

# XDebug port
EXPOSE 9000

VOLUME /app
WORKDIR /app

CMD ["/sbin/my_init"]

publish: composer_up tar_all tar ## Build dependencies and create tar.gz files

publish_dev: composer_up_dev ## Build dependencies (with dev)

chown: ## Change the ownership to $(USER_ID):$(GROUP_ID) of $(APP_PATH)
	$(BIN_DOCKER) exec -it $(CONTAINER_PHP) find /app ! -path "*/docker.*" ! -path "*/node_modules*" -exec chown $(USER_ID):$(GROUP_ID) {} \;

artisan: ## Run an artisan command in the running PHP container
	$(BIN_DOCKER) exec -it --user $(USER_ID):$(GROUP_ID) $(CONTAINER_PHP) php artisan $(command)

composer_up_dev: ## Composer update with require-dev
	$(BIN_DOCKER) run --rm -it --user $(USER_ID):$(GROUP_ID) -v $(APP_PATH):/app composer update --ignore-platform-reqs

composer_up: ## Composer update without require-dev
	$(BIN_DOCKER) run --rm -it --user $(USER_ID):$(GROUP_ID) -v $(APP_PATH):/app composer update --no-dev --ignore-platform-reqs

composer_install: ## Composer install
	$(BIN_DOCKER) run --rm -it --user $(USER_ID):$(GROUP_ID) -v $(APP_PATH):/app composer install --ignore-platform-reqs

npm_install: ## NPM install
	$(BIN_DOCKER) run --rm -it --user $(USER_ID):$(GROUP_ID) -v $(APP_PATH):/app -w /app node:$(NODE_VERSION) npm install

gulp: ## NPM install
	$(BIN_DOCKER) run --rm -it --user $(USER_ID):$(GROUP_ID) -v $(APP_PATH):/app -w /app node:$(NODE_VERSION) /app/node_modules/.bin/gulp

webpack_dev: ## Webpack build (dev)
	$(BIN_DOCKER) run --rm -it --user $(USER_ID):$(GROUP_ID) -v $(APP_PATH):/app -w /app node:$(NODE_VERSION) npm run dev

webpack: ## Webpack build (no dev)
	$(BIN_DOCKER) run --rm -it --user $(USER_ID):$(GROUP_ID) -v $(APP_PATH):/app -w /app node:$(NODE_VERSION) npm run prod

webpack_watch: ## Start the webpack watcher
	$(BIN_DOCKER) run --rm -it --user $(USER_ID):$(GROUP_ID) -v $(APP_PATH):/app -w /app node:$(NODE_VERSION) npm run watch

tar: ## Create tar.gz excluding resources and config
	tar $(EXCLUDE_TAR) --exclude="./resources" --exclude="./config" --exclude="./public/images" -czf $(PROJECT).min.tar.gz -C $(APP_PATH) .

tar_all: ## Create tar.gz with resources and config
	tar $(EXCLUDE_TAR) -czf $(PROJECT).all.tar.gz -C $(APP_PATH) .

tar_docker: ## Create tar.gz including docker files
	tar --exclude=".git" --exclude="./.idea" --exclude="./bower_components" --exclude="./node_modules" --exclude="logs/*" --exclude="*.tar.gz" -czf $(PROJECT).docker.tar.gz -C $(APP_PATH) .

zip: ## Create zip file
	cd $(APP_PATH) && zip -FSr $(PROJECT).all.zip ./* $(EXCLUDE_ZIP) && cd - && mv $(APP_PATH)/$(PROJECT).all.zip .

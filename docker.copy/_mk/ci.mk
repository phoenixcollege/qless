copy_copies: ## Copy *.copy to base directory (config, public, resources)
	cp -a $(APP_PATH)/config.copy/. $(APP_PATH)/config/
	cp -a $(APP_PATH)/public.copy/. $(APP_PATH)/public/
	cp -a $(APP_PATH)/resources.copy/. $(APP_PATH)/resources/

ci_phpcs: ## Run PHP CS (codesniffer) and generate report
	$(BIN_DOCKER_COMPOSE) -f $(COMPOSE_FILE_CI) run -v $(APP_PATH):/app $(CONTAINER_CI) phpcs --standard=PSR1,$(CI_CONF_DIR)/phpcs.xml --report=checkstyle --report-file=$(CI_DATA_DIR)/checkstyle.xml --extensions=php --ignore=autoload.php app config resources

ci_phpcpd: ## Run PHP CPD (copy paste detector) and generate report
	$(BIN_DOCKER_COMPOSE) -f $(COMPOSE_FILE_CI) run -v $(APP_PATH):/app $(CONTAINER_CI) phpcpd --log-pmd=($CI_DATA_DIR)/pmd-cpd.xml app

ci_phpmd: ## Run PHP MD (mess detector) and generate report
	$(BIN_DOCKER_COMPOSE) -f $(COMPOSE_FILE_CI) run -v $(APP_PATH):/app $(CONTAINER_CI) phpmd --reportfile=$(CI_DATA_DIR)/pmd.xml app,config,resources text cleancode

ci_phploc: ## Run PHP LOC (lines of code) and generate report
	$(BIN_DOCKER_COMPOSE) -f $(COMPOSE_FILE_CI) run -v $(APP_PATH):/app $(CONTAINER_CI) phploc --count-tests --log-csv=$(CI_DATA_DIR)/phploc.csv --log-xml=$(CI_DATA_DIR)/phploc.xml app config resources tests

ci_phpunit: ## Run PHPUnit, all tests, and generate reports including code coverage
	$(BIN_DOCKER_COMPOSE) -f $(COMPOSE_FILE_CI) run -v $(APP_PATH):/app $(CONTAINER_CI) phpunit --configuration $(REMOTE_APP_PATH)/phpunit.xml --coverage-clover $(CI_DATA_DIR)/clover.xml --coverage-crap4j $(CI_DATA_DIR)/crap4j.xml --log-junit $(CI_DATA_DIR)/junit.xml $(REMOTE_APP_PATH)/tests

ci_phpunit_unit: ## Run PHPUnit, unit tests, and generate reports including code coverage
	$(BIN_DOCKER_COMPOSE) -f $(COMPOSE_FILE_CI) run -v $(APP_PATH):/app $(CONTAINER_CI) phpunit --configuration $(REMOTE_APP_PATH)/phpunit.xml --coverage-clover $(CI_DATA_DIR)/clover.xml --coverage-crap4j $(CI_DATA_DIR)/crap4j.xml --log-junit $(CI_DATA_DIR)/junit.xml $(REMOTE_APP_PATH)/tests/unit

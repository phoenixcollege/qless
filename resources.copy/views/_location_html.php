<?php
/**
 * @var \App\Contracts\Models\Location $location
 */
?>
<div class="qless-location" id="location-<?php echo e($location->id); ?>">
    <h4><?php echo e($location->name); ?></h4>
    <?php $per_row = 3;
    $count = 0;
    $needs_end = false; ?>
    <?php $queue_count = count($location->queues); ?>
    <?php $cols = $queue_count && $queue_count <= $per_row ? 12 / $queue_count : ($queue_count && $queue_count > $per_row ? $per_row : 12); ?>
    <?php $cls = 'col-xs-12 col-md-' . $cols; ?>
    <?php foreach ($location->queues as $queue) : ?>
        <?php $state_cls = 'queue-' . $queue->state; ?>
        <?php $needs_end = true; ?>
        <?php if ($count % (12 / $per_row) === 0) : ?>
            <?php if ($count > 0) : ?>
            </div> <!-- end outer row -->
            <?php endif; ?>
            <div class="row"> <!-- begin outer row -->
        <?php endif; ?>
        <div class="<?php echo sprintf('%s %s', $cls, $state_cls); ?> qless-queue" id="queue-<?php echo e($queue->id); ?>">
            <h5 class="queue-name">
                <?php echo e($queue->name); ?>
                <div>
                    <small><?php echo e($queue->state); ?></small>
                </div>
            </h5>
            <?php if ($queue->waitInfo) : ?>
                <div class="row qless-wait-time"> <!-- begin inner row -->
                    <div class="col-sm-3 col-md-5 qless-label">
                        Wait time
                    </div>
                    <div class="col-sm-9 col-md-7 qless-value">
                        <?php echo e($queue->waitInfo->wait_time); ?>
                    </div>
                </div> <!-- end inner row -->
                <div class="row qless-in-line"> <!-- begin inner row 2 -->
                    <div class="col-sm-3 col-md-5 qless-label">
                        In line
                    </div>
                    <div class="col-sm-9 col-md-7 qless-value">
                        <?php echo e($queue->waitInfo->people_in_line); ?>
                    </div>
                </div> <!-- end inner row 2 -->
            <?php else : ?>
                <div class="text-muted">No information available</div>
            <?php endif; ?>
        </div> <!-- end queue -->
        <?php $count = $count + 1; ?>
    <?php endforeach; ?>
    <?php if ($needs_end) : ?>
        </div> <!-- end outer row -->
    <?php endif; ?>
</div> <!-- end location -->

<div class="qless-status">
    <h3>QLess
        <small>updated at <?php echo date('m/d/Y g:i:s A'); ?></small>
    </h3>
    <?php if ($results && isset($results['error']) && $results['error'] === false) : ?>
        <?php foreach ($results['results'] as $location) : ?>
            <?php require __DIR__ . DIRECTORY_SEPARATOR . '_location_html.php'; ?>
        <?php endforeach; ?>
    <?php elseif ($results && isset($results['error']) && $results['error'] === true) : ?>
        <div class="text-muted">
            There was an error loading the request:
            <?php echo e($results['results']['message']); ?>
        </div>
    <?php endif; ?>
</div>

### License

This software is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

The Laravel and Lumen frameworks are open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

### Requirements
* PHP 5.5.9+
* [Composer](https://getcomposer.org/)

Copy resources.copy, public.copy and config.copy to their respective non-copy directories

* eg, resources.copy -> resources

Edit the config/keys.php and config/qless.php files for your environment

Create IP/unique key pairs for any AJAX consumers

* If you have a server of 10.10.10.10 that needs to be able to query the ajax endpoints, add
a key:value pair for that server to be used as the key in the post data.

  * config/keys.php: `'10.10.10.10' => 'ABkasd98akasdfla'`
  * requesting server (10.10.10.10): `$.ajax({ ... data: { ... key: 'ABkasd98akasdfla' ... } ... });`
  
__AJAX endpoints__

HTML: /queues/html
JSON: /queues or /queues/json

__JSON Results__

```
{
   "status":200,
   "error":false,
   "results":[
      {
         "id":"1",
         "name":"Tim's Pizza",
         "queues":{
            "10":{
               "id":"10",
               "location_id":"1",
               "name":"2-person table",
               "waitInfo":{
                  "queue_id":"10",
                  "wait_time":"25 min",
                  "people_in_line":"6"
               }
            }
         }
      }
   ]
}
```

__Example jQuery AJAX__

```
<!doctype html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->
<div class="container" id="qless-results">
    
</div> <!-- /container -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script>
    $(document).ready(function() {
        function loadQless() {
            $.ajax({
                type: 'POST',
                dataType: 'html',
                url: 'https://my.server.edu/queues/html',
                data: {
                    location_ids: [1, 2, 10],
                    queue_ids: [100, 102],
                    key: 'abcdef'
                },
                success: function(data) {
                    $('#qless-results').html(data);
                }
            });
        }
        var reload_frequency = 1000 * 60 * 5;
        setInterval(loadQless, reload_frequency);
    });
</script>
</body>
</html>

```

<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 11/4/16
 * Time: 2:52 PM
 */

namespace App;

use App\Contracts\Models\Location;
use App\Contracts\Models\Queue;
use App\Contracts\Models\WaitInfo;

class Handler implements \App\Contracts\Handler
{

    protected $stores = [
        'location' => null,
        'queue'    => null,
        'waitinfo' => null,
    ];

    public function __construct(array $stores)
    {
        $this->stores = $stores;
    }

    public function load($location_ids, $queue_ids)
    {
        $locations = $this->getModels('location', $location_ids);
        if ($this->loadOk($locations)) {
            $queues = $this->getModels('queue', $queue_ids);
            $waitinfos = $this->getModels('waitinfo', $this->getValidQueueIds($queues));
            return $this->mergeModels($locations, $queues, $waitinfos);
        }
    }

    protected function getModels($store, $ids)
    {
        $s = array_get($this->stores, $store);
        if ($s) {
            return $s->byId($ids);
        }
    }

    protected function loadOk($results)
    {
        return $results && isset($results['error']) && $results['error'] === false;
    }

    protected function getValidQueueIds($queues)
    {
        $ids = [];
        foreach ($queues['results'] as $queue) {
            if ($queue instanceof Queue && $queue->id && $queue->state) {
                $ids[] = $queue->id;
            }
        }
        return $ids;
    }

    protected function mergeModels($locations, $queues, $waitinfos)
    {
        foreach ($locations['results'] as $location) {
            if ($location instanceof Location) {
                $this->addQueuesToLocation($location, $queues, $waitinfos);
            }
        }
        return $locations;
    }

    protected function addQueuesToLocation($location, $queues, $waitinfos)
    {
        $loc_queues = [];
        if ($this->loadOk($queues)) {
            foreach ($queues['results'] as $queue) {
                if ($queue instanceof Queue && $queue->location_id && $location->id === $queue->location_id) {
                    $loc_queues[$queue->id] = $queue;
                }
                $this->addWaitInfoToQueue($queue, $waitinfos);
            }
        }
        $location->queues = $loc_queues;
    }

    protected function addWaitInfoToQueue($queue, $waitinfos)
    {
        if ($this->loadOk($waitinfos)) {
            foreach ($waitinfos['results'] as $waitinfo) {
                if ($waitinfo instanceof WaitInfo && $waitinfo->queue_id && $waitinfo->queue_id === $queue->id) {
//                    if ($queue->state === 'closed') {
//                        $queue->state = 'open';
//                    }
                    $queue->waitInfo = $waitinfo;
                    break;
                }
            }
        }
    }
}

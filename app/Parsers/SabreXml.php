<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 11/4/16
 * Time: 10:45 AM
 */

namespace App\Parsers;

use App\Contracts\Parser;
use Sabre\Xml\Service;

class SabreXml implements Parser
{

    /**
     * @var Service
     */
    protected $service;

    public function __construct(Service $service)
    {
        $this->service = $service;
    }

    /**
     * @param $xml
     * @return array
     */
    public function parse($xml)
    {
        return $this->service->parse($xml);
    }

    public function setMap($mapping)
    {
        $this->service->elementMap = $mapping;
    }
}

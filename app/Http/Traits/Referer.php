<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 1/5/18
 * Time: 9:45 AM
 */

namespace App\Http\Traits;

use Illuminate\Http\Request;

trait Referer
{

    protected function getOriginHeader(Request $request)
    {
        $from = $request->headers->get('origin');
        if (!$from) {
            $from = $request->getClientIp();
        }
        return $from;
    }

    protected function verify(Request $request)
    {
        if (!is_null($this->getMatch($request))) {
            return true;
        }
        return false;
    }

    protected function getMatch(Request $request)
    {
        $keys = config('keys', []);
        $key = $request->get('key', null);
        $from = $this->getOriginHeader($request);
        if ($key) {
            foreach ($keys as $o => $data) {
                if ($o === $from && $key === $data['key']) {
                    return $data;
                }
            }
        }
        return null;
    }

    protected function getOrigin(Request $request)
    {
        $match = $this->getMatch($request);
        if (!is_null($match) && isset($match['origin'])) {
            return $match['origin'];
        }
        return null;
    }
}

<?php

namespace App\Http\Middleware;

use App\Http\Traits\Referer;
use Closure;

class Auth
{

    use Referer;

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($this->verify($request)) {
            return $next($request);
        }
        abort(403, "Not authorized.");
    }


}

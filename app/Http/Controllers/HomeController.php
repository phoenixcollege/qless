<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 11/4/16
 * Time: 11:25 AM
 */

namespace App\Http\Controllers;

use App\Contracts\Handler;

class HomeController extends Controller
{

    public function index(Handler $handler)
    {
        $loc_ids = config('qless.locations.ids', []);
        $queue_ids = config('qless.queues.ids', []);
        $results = $handler->load($loc_ids, $queue_ids);
        return view('home', ['results' => $results]);
    }
}

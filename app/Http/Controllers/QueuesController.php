<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 11/4/16
 * Time: 11:25 AM
 */

namespace App\Http\Controllers;

use App\Contracts\Handler;
use App\Http\Traits\Referer;
use Illuminate\Http\Request;

class QueuesController extends Controller
{

    use Referer;

    public function __construct()
    {
        $this->middleware('auth_cors');
    }

    public function load(Request $request, Handler $handler, $type = null)
    {
        //Note that we should only get here if the referer matches
        //in the auth middleware
        $headers = $this->getHeaders($request);
        $allowed = ['html', 'json'];
        $loc_ids = $request->get('location_ids', []);
        $queue_ids = $request->get('queue_ids', []);
        $results = $handler->load($loc_ids, $queue_ids);
        if ($type === null || !in_array($type, $allowed)) {
            $type = 'json';
        }
        if ($type === 'json') {
            $results['results'] = array_map(
                function ($v) {
                    return $v->toArray();
                },
                $results['results']
            );
            return response()->json($results, $results['status'], $headers);
        }
        $v = view('queue_html', ['results' => $results])->render();
        return response($v, $results['status'], $headers);
    }

    protected function getHeaders(Request $request)
    {
        $headers = config('headers', []);
        $origin = $this->getOrigin($request);
        if ($origin) {
            $headers['Access-Control-Allow-Origin'] = $origin;
        }
        return $headers;
    }
}

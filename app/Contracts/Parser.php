<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 11/4/16
 * Time: 10:44 AM
 */

namespace App\Contracts;

interface Parser
{

    /**
     * @param $xml
     * @return array
     */
    public function parse($xml);

    public function setMap($mapping);
}

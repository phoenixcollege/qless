<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 11/7/16
 * Time: 8:32 AM
 */

namespace App\Contracts\Storage;

interface Base
{

    /**
     * @param array|int $ids
     * @return array
     */
    public function byId($ids);

    /**
     * @param int $id
     * @return array
     */
    public function find($id);

    /**
     * @param $response
     * @return array
     */
    public function parse($response);
}

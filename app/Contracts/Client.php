<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 11/4/16
 * Time: 10:44 AM
 */

namespace App\Contracts;

interface Client
{

    /**
     * @param $response
     * @return bool
     */
    public function isNotAuthenticated($response);

    /**
     * @param $response
     * @return bool
     */
    public function isError($response);

    /**
     * @return mixed
     */
    public function authenticate();

    /**
     * @param string $method
     * @param string $uri
     * @param array $opts
     * @return mixed
     */
    public function request($method, $uri, $opts = []);
}

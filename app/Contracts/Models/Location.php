<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 11/4/16
 * Time: 1:23 PM
 */

namespace App\Contracts\Models;

/**
 * Interface Location
 * @package App\Contracts\Models
 *
 * @property int $id
 * @property string $name
 *
 * @property Queue[] $queues
 */
interface Location
{

}

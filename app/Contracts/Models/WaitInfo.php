<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 11/4/16
 * Time: 1:25 PM
 */

namespace App\Contracts\Models;

/**
 * Interface WaitInfo
 * @package App\Contracts\Models
 *
 * @property int $queue_id
 * @property int $wait_time
 * @property int $people_in_line
 */
interface WaitInfo
{

}

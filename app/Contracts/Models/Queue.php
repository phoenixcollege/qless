<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 11/4/16
 * Time: 1:23 PM
 */

namespace App\Contracts\Models;

/**
 * Interface Queue
 * @package App\Contracts\Models
 *
 * @property int $id
 * @property int $location_id
 * @property string $name
 * @property string $state
 * @property WaitInfo $waitInfo
 */
interface Queue
{

}

<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 11/4/16
 * Time: 2:56 PM
 */

namespace App\Contracts;

interface Handler
{

    public function load($location_ids, $queue_ids);
}

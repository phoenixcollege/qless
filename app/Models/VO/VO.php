<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 11/4/16
 * Time: 1:23 PM
 */

namespace App\Models\VO;

class VO
{

    protected $attributes = [];

    public function __construct(array $attributes = [])
    {
        if ($attributes) {
            $this->setAttributes($attributes);
        }
    }

    public function newInstance(array $attributes = [])
    {
        return new static($attributes);
    }

    /**
     * Returns the value of the property if it exists in $this->attributes
     * @param $key
     * @return mixed
     */
    public function __get($key)
    {
        return $this->getAttribute($key);
    }

    /**
     * Creates a new value in $this->attributes
     * @param $key
     * @param $value
     */
    public function __set($key, $value)
    {
        $this->setAttribute($key, $value);
    }

    public function getAttribute($key, $default = null)
    {
        $methodname = 'get' . studly_case($key);
        if (method_exists($this, $methodname)) {
            return $this->$methodname();
        }
        if ($this->has($key)) {
            return $this->attributes[$key];
        }
        return $default;
    }

    public function has($key)
    {
        if (array_key_exists($key, $this->attributes)) {
            return true;
        }
        return false;
    }

    public function setAttribute($key, $value)
    {
        $methodname = 'set' . studly_case($key);
        if (method_exists($this, $methodname)) {
            $this->$methodname($value);
        } else {
            $this->attributes[$key] = $value;
        }
    }

    public function __isset($key)
    {
        return $this->has($key);
    }

    public function toArray($val = null)
    {
        $arr = [];
        if (is_null($val)) {
            $val = $this->getAttributes();
        }
        foreach ($val as $k => $v) {
            $v = $this->getAttribute($k) ?: $v;
            if (is_array($v)) {
                $arr[$k] = $this->toArray($v);
            } elseif ($v instanceof VO) {
                $arr[$k] = $v->toArray();
            } else {
                $arr[$k] = $v;
            }
        }
        return $arr;
    }

    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * Mass set $this->attributes
     * @param array $attributes
     */
    public function setAttributes(array $attributes)
    {
        $this->attributes = array_merge($this->attributes, $attributes);
    }
}

<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 11/4/16
 * Time: 1:22 PM
 */

namespace App\Models\VO;

class Queue extends VO implements \App\Contracts\Models\Queue
{

    public function getState()
    {
        $conversion = [
            'active' => 'open',
            'inactive' => 'closed',
            'closed' => 'closed',
            'closing' => 'closed',
        ];
        $v = array_get($this->attributes, 'state', null);
        if (isset($conversion[$v])) {
            return $conversion[$v];
        }
        return $v;
    }
}

<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 11/4/16
 * Time: 11:04 AM
 */

namespace App\Providers;

use App\Clients\Guzzle;
use GuzzleHttp\Client;
use Illuminate\Support\ServiceProvider;

class ClientServiceProvider extends ServiceProvider
{

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Contracts\Client',
            function ($app) {
                $client_cls = Client::class;
                return new Guzzle($client_cls, config('qless', []));
            }
        );
    }
}

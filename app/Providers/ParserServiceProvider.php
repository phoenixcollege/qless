<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 11/4/16
 * Time: 11:04 AM
 */

namespace App\Providers;

use App\Parsers\SabreXml;
use Illuminate\Support\ServiceProvider;
use Sabre\Xml\Service;

class ParserServiceProvider extends ServiceProvider
{

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Contracts\Parser',
            function ($app) {
                $s = new Service();
                return new SabreXml($s);
            }
        );
    }
}

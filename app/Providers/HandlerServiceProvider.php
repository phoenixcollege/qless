<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 11/4/16
 * Time: 2:56 PM
 */

namespace App\Providers;

use App\Handler;
use Illuminate\Support\ServiceProvider;

class HandlerServiceProvider extends ServiceProvider
{

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Contracts\Handler',
            function ($app) {
                $stores = [
                    'location' => $app['App\Contracts\Storage\Location'],
                    'queue'    => $app['App\Contracts\Storage\Queue'],
                    'waitinfo' => $app['App\Contracts\Storage\WaitInfo'],
                ];
                return new Handler($stores);
            }
        );
    }
}

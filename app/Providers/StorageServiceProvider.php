<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 11/7/16
 * Time: 6:58 AM
 */

namespace App\Providers;

use App\Models\VO\Location;
use App\Models\VO\Queue;
use App\Models\VO\WaitInfo;
use Illuminate\Support\ServiceProvider;

class StorageServiceProvider extends ServiceProvider
{

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $options = config('qless', []);
        $client = $this->app['App\Contracts\Client'];
        $parser = $this->app['App\Contracts\Parser'];
        $this->location($options, $client, $parser);
        $this->queue($options, $client, $parser);
        $this->waitInfo($options, $client, $parser);
    }

    protected function location($options, $client, $parser)
    {
        $o = array_get($options, 'locations', []);
        $this->app->bind(
            'App\Contracts\Storage\Location',
            function ($app) use ($o, $client, $parser) {
                $m = new Location();
                return new \App\Storage\Guzzle\Location($m, $client, $parser, $o);
            }
        );
    }

    protected function queue($options, $client, $parser)
    {
        $o = array_get($options, 'queues', []);
        $this->app->bind(
            'App\Contracts\Storage\Queue',
            function ($app) use ($o, $client, $parser) {
                $m = new Queue();
                return new \App\Storage\Guzzle\Queue($m, $client, $parser, $o);
            }
        );
    }

    protected function waitInfo($options, $client, $parser)
    {
        $o = array_get($options, 'waitinfo', []);
        $this->app->bind(
            'App\Contracts\Storage\WaitInfo',
            function ($app) use ($o, $client, $parser) {
                $m = new WaitInfo();
                return new \App\Storage\Guzzle\WaitInfo($m, $client, $parser, $o);
            }
        );
    }
}

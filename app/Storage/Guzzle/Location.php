<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 11/7/16
 * Time: 7:03 AM
 */

namespace App\Storage\Guzzle;

class Location extends Base implements \App\Contracts\Storage\Location
{

    protected function setMap()
    {
        $this->getParser()->setMap(
            [
                '{}merchantLocations' => function ($reader) {
                    $locations = [];
                    $children = $reader->parseInnerTree();
                    if ($children) {
                        foreach ($children as $child) {
                            if ($child['value'] instanceof \App\Contracts\Models\Location) {
                                $locations[] = $child['value'];
                            }
                        }
                    }
                    return $locations;
                },
                '{}merchantLocation'  => function ($reader) {
                    $data = [
                        'id'     => null,
                        'name'   => null,
                        'queues' => [],
                    ];
                    $atts = $reader->parseAttributes();

                    $kv = \Sabre\Xml\Deserializer\keyValue($reader, '{}');
                    if (isset($kv['{}description'])) {
                        $data['name'] = $kv['{}description'];
                    }
                    if (isset($atts['id'])) {
                        $data['id'] = $atts['id'];
                    }
                    return $this->getModel()->newInstance($data);
                },
            ]
        );
    }
}

<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 11/7/16
 * Time: 6:55 AM
 */

namespace App\Storage\Guzzle;

use App\Contracts\Client;
use App\Contracts\Parser;
use App\Models\VO\VO;
use GuzzleHttp\Exception\ConnectException;

abstract class Base
{

    /**
     * @var VO
     */
    protected $model;

    /**
     * @var Client
     */
    protected $client;

    /**
     * @var Parser
     */
    protected $parser;

    protected $options = [];

    protected $cache_time = 480;

    public function __construct(VO $model, Client $client, Parser $parser, array $options)
    {
        $this->setModel($model);
        $this->setClient($client);
        $this->setParser($parser);
        $this->options = $options;
    }

    public function find($id)
    {
        return $this->byId($id);
    }

    public function byId($ids)
    {
        $ids = $this->getAllowedIds($ids);
        if (!$ids) {
            abort(404, "No valid IDs found.");
        }
        $key = sprintf('%s/byId/%s', class_basename($this), implode('/', $ids));
        $last_value = \Cache::get($key);
        $this->shouldInvalidateCache($key, $last_value);
        return \Cache::remember(
            $key,
            $this->cache_time,
            function () use ($ids, $last_value) {
                $uri = array_get($this->options, 'uri');
                if ($uri) {
                    $uri = sprintf($uri, implode(',', $ids));
                    return $this->handleResponse($uri, $last_value);
                }
            }
        );
    }

    protected function shouldInvalidateCache($key, $last_value)
    {
        if (!$last_value ||
            (isset($last_value['results']) && !$last_value['results']) ||
            (isset($last_value['status']) && $last_value['status'] !== 200)
        ) {
            \Cache::forget($key);
        }
    }

    protected function handleResponse($uri, $last_value)
    {
        try {
            $response = $this->getClient()->request('GET', $uri);
        } catch (ConnectException $e) {
            \Log::warning($e);
            $response = null;
            if ($last_value) {
                return $last_value;
            }
        }
        return $this->parse($response);
    }

    protected function getAllowedIds($request_ids)
    {
        $ids = [];
        $allowed_ids = array_get($this->options, 'ids', []);
        if ($request_ids) {
            $request_ids = (array)$request_ids;
            foreach ($request_ids as $rid) {
                $id = (int)$rid;
                if ($id && in_array($id, $allowed_ids)) {
                    $ids[] = $id;
                }
            }
        }
        return $ids;
    }

    public function parse($response)
    {
        $status = $response ? $response->getStatusCode() : 200;
        $result = [
            'status'  => $status,
            'error'   => false,
            'results' => [
            ],
        ];
        if ($status === 200) {
            $result['results'] = $this->responseParse($response);
        } else {
            $result['error'] = true;
            $result['results']['message'] = $response->getReasonPhrase();
        }
        return $result;
    }

    protected function responseParse($response)
    {
        $body = $response ? (string)$response->getBody() : '<none/>';
        $this->setMap();
        $parsed = $this->getParser()->parse($body);
        return $parsed ?: [];
    }

    abstract protected function setMap();

    /**
     * @return Client
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @param Client $client
     */
    public function setClient($client)
    {
        $this->client = $client;
    }

    /**
     * @return VO
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * @param VO $model
     */
    public function setModel($model)
    {
        $this->model = $model;
    }

    /**
     * @return Parser
     */
    public function getParser()
    {
        return $this->parser;
    }

    /**
     * @param Parser $parser
     */
    public function setParser($parser)
    {
        $this->parser = $parser;
    }
}

<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 11/7/16
 * Time: 7:04 AM
 */

namespace App\Storage\Guzzle;

class Queue extends Base implements \App\Contracts\Storage\Queue
{

    protected $cache_time = 5;

    protected function setMap()
    {
        $this->getParser()->setMap(
            [
                '{}queues' => function ($reader) {
                    $qs = [];
                    $children = $reader->parseInnerTree();
                    if ($children) {
                        foreach ($children as $child) {
                            if ($child['value'] instanceof \App\Contracts\Models\Queue) {
                                $qs[] = $child['value'];
                            }
                        }
                    }
                    return $qs;
                },
                '{}queue'  => function ($reader) {
                    $data = [
                        'id'          => null,
                        'location_id' => null,
                        'name'        => null,
                        'state'       => null,
                        'waitInfo'    => null,
                    ];
                    $atts = $reader->parseAttributes();
                    $kv = \Sabre\Xml\Deserializer\keyValue($reader, '{}');
                    if (isset($kv['{}description'])) {
                        $data['name'] = $kv['{}description'];
                    }
                    if (isset($atts['id'])) {
                        $data['id'] = $atts['id'];
                    }
                    if (isset($atts['state'])) {
                        $data['state'] = strtolower($atts['state']);
                    }
                    if (isset($atts['locationId'])) {
                        $data['location_id'] = $atts['locationId'];
                    }
                    return $this->getModel()->newInstance($data);
                },
            ]
        );
    }
}

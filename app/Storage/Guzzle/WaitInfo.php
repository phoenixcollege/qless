<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 11/7/16
 * Time: 8:19 AM
 */

namespace App\Storage\Guzzle;

class WaitInfo extends Base implements \App\Contracts\Storage\WaitInfo
{

    protected $cache_time = 1;

    protected function setMap()
    {
        $this->getParser()->setMap(
            [
                '{}waitInfos' => function ($reader) {
                    $wis = [];
                    $children = $reader->parseInnerTree();
                    if ($children) {
                        foreach ($children as $child) {
                            if ($child['value'] instanceof \App\Contracts\Models\WaitInfo) {
                                $wis[] = $child['value'];
                            }
                        }
                    }
                    return $wis;
                },
                '{}waitInfo'  => function ($reader) {
                    $data = [
                        'queue_id'       => null,
                        'wait_time'      => null,
                        'people_in_line' => null,
                    ];
                    $atts = $reader->parseAttributes();
                    $kv = \Sabre\Xml\Deserializer\keyValue($reader, '{}');
                    if (isset($kv['{}forecastNextWaitTime'])) {
                        $data['wait_time'] = $kv['{}forecastNextWaitTime'];
                    }
                    if (isset($atts['queueId'])) {
                        $data['queue_id'] = $atts['queueId'];
                    }
                    if (isset($atts['peopleInLine'])) {
                        $data['people_in_line'] = $atts['peopleInLine'];
                    }
                    return $this->getModel()->newInstance($data);
                },
            ]
        );
    }
}

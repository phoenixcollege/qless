<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 11/4/16
 * Time: 10:45 AM
 */

namespace App\Clients;

use App\Contracts\Client;
use GuzzleHttp\Cookie\SessionCookieJar;

class Guzzle implements Client
{

    protected $client_cls;

    /**
     * @var \GuzzleHttp\Client
     */
    protected $client;

    protected $options = [];

    public function __construct($client_cls, $options = [])
    {
        $this->client_cls = $client_cls;
        $this->options = $options;
    }

    public function isError($response)
    {
        $code = $response->getStatusCode();
        return ($code > 404 || $code === 400);
    }

    public function request($method, $uri, $opts = [])
    {
        $response = $this->getClient()->request($method, $uri, $opts);
        return $this->verifyResponse($response, $method, $uri, $opts);
    }

    protected function verifyResponse($response, $method, $uri, $opts)
    {
        if (!$this->isNotAuthenticated($response)) {
            return $response;
        }
        $r = $this->authenticate();
        if ($this->isNotAuthenticated($r)) {
            return $r;
        }
        return $this->getClient()->request($method, $uri, $opts);
    }

    /**
     * @param $response
     * @return bool
     */
    public function isNotAuthenticated($response)
    {
        $code = $response->getStatusCode();
        return ($code === 401 || $code === 403);
    }

    /**
     * @return bool
     */
    public function authenticate()
    {
        $auth = array_get($this->options, 'authentication', []);
        $post = [
            'principal'   => array_get($auth, 'principal'),
            'credentials' => array_get($auth, 'credentials'),
            'remember'    => 'true',
        ];
        return $this->getClient()->request(
            'POST',
            array_get($auth, 'uri', 'authenticator'),
            [
                'form_params' => $post,
            ]
        );
    }

    /**
     * @return \GuzzleHttp\Client
     */
    protected function getClient()
    {
        if (!$this->client) {
            if (is_object($this->client_cls)) {
                $this->client = $this->client_cls;
            } else {
                $opts = array_get($this->options, 'client_options', []);
                $opts = $this->addCookieJar($opts);
                $rc = new \ReflectionClass($this->client_cls);
                $this->client = $rc->newInstance($opts);
            }
        }
        return $this->client;
    }

    protected function addCookieJar($options)
    {
        if (isset($options['cookies']) && $options['cookies'] === true) {
            $options['cookies'] = new SessionCookieJar('QLESS_SESSION', true);
        }
        return $options;
    }
}

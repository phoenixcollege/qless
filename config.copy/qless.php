<?php
return [
    'client_options' => [
        'base_uri' => env('QLESS_URI', 'https://merchant.us1.qless.com'),
        'timeout'  => 2,
        'cookie'   => true,
        'verify'   => false,
    ],
    'authentication' => [
        'uri'         => 'qless/authenticator',
        'principal'   => env('QLESS_PRINCIPAL', null),
        'credentials' => env('QLESS_CREDENTIALS', null),
    ],
    'locations'      => [
        'uri' => 'qless/api/v1/kiosk/locations/%s',
        'ids' => [
            122, //Some location
            123,
        ],
    ],
    'queues'         => [
        'uri' => 'qless/api/v1/kiosk/queues/%s',
        'ids' => [
            456, //Some queue
            4556,
        ],
    ],
    'waitinfo'       => [
        'uri' => 'qless/api/v1/kiosk/queues/%s/waitInfo',
        'ids' => [
            456, //Some queue
            4556,
        ],
    ],
];

<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 11/7/16
 * Time: 10:47 AM
 */

namespace App\Tests\Unit\Models;

use App\Models\VO\VO;

class VOTest extends \PHPUnit\Framework\TestCase
{

    public function testToArraySimple()
    {
        $vo = new VO(['foo' => 'bar']);
        $this->assertEquals(['foo' => 'bar'], $vo->toArray());
    }

    public function testToArrayWithArray()
    {
        $data = [
            'foo' => 'bar',
            'biz' => [
                'buz',
                'baz',
            ],
        ];
        $vo = new VO($data);
        $this->assertEquals($data, $vo->toArray());
    }

    public function testToArrayWithArrayAndVO()
    {
        $data = [
            'foo' => 'bar',
            'biz' => [
                'buz',
                'baz',
            ],
            'fuz' => new VO(['a' => 'b']),
        ];
        $expected = $data;
        $expected['fuz'] = ['a' => 'b'];
        $vo = new VO($data);
        $this->assertEquals($expected, $vo->toArray());
    }

    public function testToArrayWithArrayOfVOs()
    {
        $data = [
            'foo' => 'bar',
            'biz' => [
                'buz',
                'baz',
            ],
            'fuz' => [
                new VO(['a' => 'b']),
                new VO(['c' => 'd']),
            ],
        ];
        $expected = $data;
        $expected['fuz'] = [['a' => 'b'], ['c' => 'd']];
        $vo = new VO($data);
        $this->assertEquals($expected, $vo->toArray());
    }
}

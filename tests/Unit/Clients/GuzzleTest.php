<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 11/7/16
 * Time: 8:03 AM
 */

namespace App\Tests\Unit\Clients;

use App\Clients\Guzzle;
use App\Contracts\Client;
use Mockery as m;

class GuzzleTest extends \PHPUnit\Framework\TestCase
{

    public function tearDown()
    {
        m::close();
    }

    public function testClientInstanceOfClient()
    {
        list($sut, $c) = $this->getSut();
        $this->assertInstanceOf(Client::class, $sut);
    }

    public function getSut($opts = [])
    {
        $c = m::mock('\GuzzleHttp\Client');
        $opts = $this->getOpts($opts);
        $g = new Guzzle($c, $opts);
        return [$g, $c];
    }

    protected function getOpts($opts = [])
    {
        $o = [
            'client_options' => [
                'base_uri' => env('QLESS_URI', 'https://merchant.qless.com/qless'),
                'timeout'  => 2,
                'cookie'   => true,
            ],
            'authentication' => [
                'uri'         => 'authenticator',
                'principal'   => 'foop',
                'credentials' => 'fooc',
            ],
            'locations'      => [
                'uri' => 'api/v1/employee/locations/%s',
                'ids' => [
                    1, //Location Name
                ],
            ],
            'queues'         => [
                'uri' => 'api/v1/employee/queues/%s',
                'ids' => [
                    1, // queue name
                ],
            ],
            'waitinfo'       => [
                'uri' => 'api/v1/kiosk/queues/%s/waitInfo',
            ],
        ];
        return array_replace_recursive($o, $opts);
    }

    public function testRequestOk()
    {
        list($sut, $c) = $this->getSut();
        $r = m::mock('Response');
        $c->shouldReceive('request')->once()->with('GET', 'foo', [])->andReturn($r);
        $r->shouldReceive('getStatusCode')->andReturn(200);
        $this->assertEquals($r, $sut->request('GET', 'foo'));
    }

    public function testRequestErrorReturnsErrorResponse()
    {
        list($sut, $c) = $this->getSut();
        $r = m::mock('Response');
        $c->shouldReceive('request')->once()->with('GET', 'foo', [])->andReturn($r);
        $r->shouldReceive('getStatusCode')->andReturn(500);
        $this->assertEquals($r, $sut->request('GET', 'foo'));
    }

    public function testRequestNeedsAuthWillAuthAndReturnResponse()
    {
        list($sut, $c) = $this->getSut();
        $r = m::mock('Response');
        $authres = m::mock('AuthResponse');
        $c->shouldReceive('request')->twice()->with('GET', 'foo', [])->andReturn($r);
        $r->shouldReceive('getStatusCode')->andReturn(401);
        $c->shouldReceive('request')->once()->with(
            'POST',
            'authenticator',
            [
                'form_params' => [
                    'principal'   => 'foop',
                    'credentials' => 'fooc',
                    'remember'    => 'true',
                ],
            ]
        )
          ->andReturn($authres);
        $authres->shouldReceive('getStatusCode')->andReturn(200);
        $this->assertEquals($r, $sut->request('GET', 'foo'));
    }

    public function testRequestNeedsAuthAndAuthFailsReturnsAuthResponse()
    {
        list($sut, $c) = $this->getSut();
        $r = m::mock('Response');
        $authres = m::mock('AuthResponse');
        $c->shouldReceive('request')->once()->with('GET', 'foo', [])->andReturn($r);
        $r->shouldReceive('getStatusCode')->andReturn(401);
        $c->shouldReceive('request')->once()->with(
            'POST',
            'authenticator',
            [
                'form_params' => [
                    'principal'   => 'foop',
                    'credentials' => 'fooc',
                    'remember'    => 'true',
                ],
            ]
        )
          ->andReturn($authres);
        $authres->shouldReceive('getStatusCode')->andReturn(403);
        $this->assertEquals($authres, $sut->request('GET', 'foo'));
    }
}

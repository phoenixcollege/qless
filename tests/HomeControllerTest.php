<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 11/4/16
 * Time: 11:36 AM
 */

namespace App\Tests;

use App\Clients\Guzzle;
use App\Providers\HandlerServiceProvider;
use App\Providers\StorageServiceProvider;
use Mockery as m;

class HomeControllerTest extends TestCase
{

    protected $mock_client;

    public function setUp()
    {
        parent::setUp();
        $this->setupConfig();
        $client_cls = m::mock('GuzzleClient');
        $this->mock_client = $client_cls;
        $client = new Guzzle($client_cls, config('qless', []));
        $this->app['App\Contracts\Client'] = $client;
        $sp = new StorageServiceProvider($this->app);
        $sp->register();
        $hp = new HandlerServiceProvider($this->app);
        $hp->register();
    }

    public function tearDown()
    {
        parent::tearDown();
        m::close();
    }

    public function testHomeRoute()
    {
        $loc_resp = m::mock('ResponseLocation');
        $q_resp = m::mock('ResponseQueue');
        $wi_resp = m::mock('ResponseWaitInfo');
        $this->mock_client->shouldReceive('request')->once()->with(
            'GET',
            'api/v1/kiosk/locations/1,2,3',
            []
        )->andReturn($loc_resp);
        $loc_resp->shouldReceive('getStatusCode')->twice()->andReturn(200);
        $loc_resp->shouldReceive('getBody')->once()->andReturn($this->getXml('locations.xml'));
        $this->mock_client->shouldReceive('request')->once()->with(
            'GET',
            'api/v1/kiosk/queues/10,11',
            []
        )->andReturn($q_resp);
        $q_resp->shouldReceive('getStatusCode')->twice()->andReturn(200);
        $q_resp->shouldReceive('getBody')->once()->andReturn($this->getXml('queues.xml'));
        $this->mock_client->shouldReceive('request')->once()->with(
            'GET',
            'api/v1/kiosk/queues/10,11/waitInfo',
            []
        )->andReturn($wi_resp);
        $wi_resp->shouldReceive('getStatusCode')->twice()->andReturn(200);
        $wi_resp->shouldReceive('getBody')->once()->andReturn($this->getXml('waitInfos.xml'));
        $r = $this->call('GET', '/');
        $this->assertEquals(200, $r->status());
        $this->assertContains('2-person table', $r->getContent());
    }

    protected function getXml($file)
    {
        return file_get_contents(__DIR__ . '/responses/' . $file);
    }

    protected function setupConfig()
    {
        config(
            [
                'qless' =>
                    [
                        'client_options' => [
                            'base_uri' => 'https://merchant.qless.com/qless',
                            'timeout'  => 2,
                            'cookie'   => true,
                        ],
                        'authentication' => [
                            'uri'         => 'authenticator',
                            'principal'   => 'foo',
                            'credentials' => 'bar',
                        ],
                        'locations'      => [
                            'uri' => 'api/v1/kiosk/locations/%s',
                            'ids' => [
                                1, //Location Name
                                2,
                                3,
                            ],
                        ],
                        'queues'         => [
                            'uri' => 'api/v1/kiosk/queues/%s',
                            'ids' => [
                                10, // queue name
                                11,
                            ],
                        ],
                        'waitinfo'       => [
                            'uri' => 'api/v1/kiosk/queues/%s/waitInfo',
                            'ids' => [
                                10, // queue name
                                11,
                            ],
                        ],
                    ],
            ]
        );
    }
}

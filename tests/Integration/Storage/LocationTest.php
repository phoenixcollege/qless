<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 11/7/16
 * Time: 8:49 AM
 */

namespace App\Tests\Integration\Storage;

use App\Models\VO\Location;
use App\Parsers\SabreXml;
use App\Tests\TestCase;
use GuzzleHttp\Exception\ConnectException;
use Mockery as m;
use Psr\Http\Message\RequestInterface;
use Sabre\Xml\Service;

class LocationTest extends TestCase
{

    public function tearDown()
    {
        parent::tearDown();
        m::close();
    }

    public function testInvalidIDIsRemoved()
    {
        list($sut, $c) = $this->getSut();
        $response = m::mock('Response');
        $c->shouldReceive('request')->once()->with('GET', 'api/v1/kiosk/locations/1,2')->andReturn($response);
        $response->shouldReceive('getStatusCode')->once()->andReturn(200);
        $response->shouldReceive('getBody')->once()->andReturn('<foos></foos>');
        $r = $sut->byId([1, 2, 99]);
        $expected = [
            'status'  => 200,
            'error'   => false,
            'results' => [],
        ];
        $this->assertEquals($expected, $r);
    }

    public function testConnectErrorReturnsNoData()
    {
        list($sut, $c) = $this->getSut();
        $request = m::mock(RequestInterface::class);
        $c->shouldReceive('request')->once()->with('GET', 'api/v1/kiosk/locations/1,2')->andThrow(new ConnectException('Timed out.', $request));
        $r = $sut->byId([1, 2]);
        $expected = [
            'status'  => 200,
            'error'   => false,
            'results' => [],
        ];
        $this->assertEquals($expected, $r);
    }

    public function testCachedReturnsCachedData()
    {
        list($sut, $c) = $this->getSut();
        $response = m::mock('Response');
        $c->shouldReceive('request')->once()->with('GET', 'api/v1/kiosk/locations/1,2')->andReturn($response);
        $response->shouldReceive('getStatusCode')->once()->andReturn(200);
        $response->shouldReceive('getBody')->once()->andReturn($this->getXml('locations.xml'));
        $r = $sut->byId([1, 2]);
        $r = $sut->byId([1, 2]);
        $this->assertEquals(200, $r['status']);
        $this->assertCount(2, $r['results']);
        $this->assertInstanceOf(\App\Contracts\Models\Location::class, $r['results'][0]);
    }

    public function testCachedNothingInvalidatesCachedData()
    {
        list($sut, $c) = $this->getSut();
        $response = m::mock('Response');
        $c->shouldReceive('request')->twice()->with('GET', 'api/v1/kiosk/locations/1,2')->andReturn($response);
        $response->shouldReceive('getStatusCode')->twice()->andReturn(200);
        $response->shouldReceive('getBody')->twice()->andReturn('<foo/>');
        $r = $sut->byId([1, 2]);
        $r = $sut->byId([1, 2]);
        $expected = [
            'status'  => 200,
            'error'   => false,
            'results' => [],
        ];
        $this->assertEquals($expected, $r);
    }

    protected function getSut($opts = [])
    {
        $vo = new Location();
        $c = m::mock('App\Contracts\Client');
        $p = new SabreXml(new Service());
        $sut = new \App\Storage\Guzzle\Location($vo, $c, $p, $this->getOpts($opts));
        return [$sut, $c];
    }

    protected function getOpts($opts = [])
    {
        $o = [
            'uri' => 'api/v1/kiosk/locations/%s',
            'ids' => [
                1, //test loc 1
                2, //test loc 2
                3, //test loc 3
            ],
        ];
        return array_replace_recursive($o, $opts);
    }

    public function testErrorReturnsErrorData()
    {
        list($sut, $c) = $this->getSut();
        $response = m::mock('Response');
        $c->shouldReceive('request')->once()->with('GET', 'api/v1/kiosk/locations/1')->andReturn($response);
        $response->shouldReceive('getStatusCode')->once()->andReturn(500);
        $response->shouldReceive('getReasonPhrase')->once()->andReturn('Oops!');
        $r = $sut->byId(1);
        $expected = [
            'status'  => 500,
            'error'   => true,
            'results' => [
                'message' => 'Oops!',
            ],
        ];
        $this->assertEquals($expected, $r);
    }

    public function testOkWithResults()
    {
        list($sut, $c) = $this->getSut();
        $response = m::mock('Response');
        $c->shouldReceive('request')->once()->with('GET', 'api/v1/kiosk/locations/1,2')->andReturn($response);
        $response->shouldReceive('getStatusCode')->once()->andReturn(200);
        $response->shouldReceive('getBody')->once()->andReturn($this->getXml('locations.xml'));
        $r = $sut->byId([1, 2]);
        $this->assertEquals(1, $r['results'][0]->id);
        $this->assertEquals("Tim's Pizza", $r['results'][0]->name);
        $this->assertEquals(2, $r['results'][1]->id);
        $this->assertEquals("Timmy's Pizza", $r['results'][1]->name);
    }

    protected function getXml($file)
    {
        return file_get_contents(__DIR__ . '/../../responses/' . $file);
    }
}

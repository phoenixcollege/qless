<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 11/7/16
 * Time: 8:49 AM
 */

namespace App\Tests\Integration\Storage;

use App\Models\VO\Queue;
use App\Parsers\SabreXml;
use App\Tests\TestCase;
use Mockery as m;
use Sabre\Xml\Service;

class QueueTest extends TestCase
{

    public function tearDown()
    {
        parent::tearDown();
        m::close();
    }

    public function testInvalidIDIsRemoved()
    {
        list($sut, $c) = $this->getSut();
        $response = m::mock('Response');
        $c->shouldReceive('request')->once()->with('GET', 'api/v1/kiosk/queues/10,11')->andReturn($response);
        $response->shouldReceive('getStatusCode')->once()->andReturn(200);
        $response->shouldReceive('getBody')->once()->andReturn('<foos></foos>');
        $r = $sut->byId([10, 11, 99]);
        $expected = [
            'status'  => 200,
            'error'   => false,
            'results' => [],
        ];
        $this->assertEquals($expected, $r);
    }

    protected function getSut($opts = [])
    {
        $vo = new Queue();
        $c = m::mock('App\Contracts\Client');
        $p = new SabreXml(new Service());
        $sut = new \App\Storage\Guzzle\Queue($vo, $c, $p, $this->getOpts($opts));
        return [$sut, $c];
    }

    protected function getOpts($opts = [])
    {
        $o = [
            'uri' => 'api/v1/kiosk/queues/%s',
            'ids' => [
                10, //test q 1
                11, //test q 2
            ],
        ];
        return array_replace_recursive($o, $opts);
    }

    public function testErrorReturnsErrorData()
    {
        list($sut, $c) = $this->getSut();
        $response = m::mock('Response');
        $c->shouldReceive('request')->once()->with('GET', 'api/v1/kiosk/queues/10')->andReturn($response);
        $response->shouldReceive('getStatusCode')->once()->andReturn(500);
        $response->shouldReceive('getReasonPhrase')->once()->andReturn('Oops!');
        $r = $sut->byId(10);
        $expected = [
            'status'  => 500,
            'error'   => true,
            'results' => [
                'message' => 'Oops!',
            ],
        ];
        $this->assertEquals($expected, $r);
    }

    public function testOkWithResults()
    {
        list($sut, $c) = $this->getSut();
        $response = m::mock('Response');
        $c->shouldReceive('request')->once()->with('GET', 'api/v1/kiosk/queues/10,11')->andReturn($response);
        $response->shouldReceive('getStatusCode')->once()->andReturn(200);
        $response->shouldReceive('getBody')->once()->andReturn($this->getXml('queues.xml'));
        $r = $sut->byId([10, 11]);
        $this->assertEquals(10, $r['results'][0]->id);
        $this->assertEquals("2-person table", $r['results'][0]->name);
        $this->assertEquals(1, $r['results'][0]->location_id);
        $this->assertEquals(11, $r['results'][1]->id);
        $this->assertEquals("4-person table", $r['results'][1]->name);
        $this->assertEquals(1, $r['results'][1]->location_id);
    }

    protected function getXml($file)
    {
        return file_get_contents(__DIR__ . '/../../responses/' . $file);
    }
}

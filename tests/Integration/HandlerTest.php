<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 11/7/16
 * Time: 10:22 AM
 */

namespace App\Tests\Integration;

use App\Handler;
use App\Models\VO\Location;
use App\Models\VO\Queue;
use App\Models\VO\WaitInfo;
use App\Parsers\SabreXml;
use App\Tests\TestCase;
use Mockery as m;
use Sabre\Xml\Service;

class HandlerTest extends TestCase
{

    public function tearDown()
    {
        parent::tearDown();
        m::close();
    }

    public function testMergeResults()
    {
        list($sut, $c) = $this->getSut();
        $resp_locs = m::mock('ResponseLocations');
        $c->shouldReceive('request')->once()->with('GET', 'api/v1/kiosk/locations/1,2,3')->andReturn($resp_locs);
        $resp_locs->shouldReceive('getStatusCode')->once()->andReturn(200);
        $resp_locs->shouldReceive('getBody')->once()->andReturn($this->getXml('locations.xml'));
        $resp_qs = m::mock('ResponseQueues');
        $c->shouldReceive('request')->once()->with('GET', 'api/v1/kiosk/queues/10,11')->andReturn($resp_qs);
        $resp_qs->shouldReceive('getStatusCode')->once()->andReturn(200);
        $resp_qs->shouldReceive('getBody')->once()->andReturn($this->getXml('queues.xml'));
        $resp_wi = m::mock('ResponseWaitInfos');
        $c->shouldReceive('request')->once()->with('GET', 'api/v1/kiosk/queues/10,11/waitInfo')->andReturn($resp_wi);
        $resp_wi->shouldReceive('getStatusCode')->once()->andReturn(200);
        $resp_wi->shouldReceive('getBody')->once()->andReturn($this->getXml('waitInfos.xml'));
        $locs = $sut->load([1, 2, 3], [10, 11]);
        $this->assertCount(2, $locs['results']);
        $this->assertCount(2, $locs['results'][0]->queues);
        $this->assertEquals('25 min', $locs['results'][0]->queues[10]->waitInfo->wait_time);
        $this->assertCount(0, $locs['results'][1]->queues);
    }

    protected function getSut()
    {
        $c = m::mock('\App\Contracts\Client');
        $stores = [
            'location' => $this->getLocation($c),
            'queue'    => $this->getQueue($c),
            'waitinfo' => $this->getWaitInfo($c),
        ];
        $sut = new Handler($stores);
        return [$sut, $c];
    }

    protected function getLocation($client)
    {
        $vo = new Location();
        $p = new SabreXml(new Service());
        $sut = new \App\Storage\Guzzle\Location(
            $vo, $client, $p, [
                   'uri' => 'api/v1/kiosk/locations/%s',
                   'ids' => [
                       1, //test loc 1
                       2, //test loc 2
                       3, //test loc 3
                   ],
               ]
        );
        return $sut;
    }

    protected function getQueue($client)
    {
        $vo = new Queue();
        $p = new SabreXml(new Service());
        $sut = new \App\Storage\Guzzle\Queue(
            $vo, $client, $p, [
                   'uri' => 'api/v1/kiosk/queues/%s',
                   'ids' => [
                       10, //test q 1
                       11, //test q 2
                   ],
               ]
        );
        return $sut;
    }

    protected function getWaitInfo($client)
    {
        $vo = new WaitInfo();
        $p = new SabreXml(new Service());
        $sut = new \App\Storage\Guzzle\WaitInfo(
            $vo, $client, $p, [
                   'uri' => 'api/v1/kiosk/queues/%s/waitInfo',
                   'ids' => [
                       10, //test q 1
                       11, //test q 2
                   ],
               ]
        );
        return $sut;
    }

    protected function getXml($file)
    {
        return file_get_contents(__DIR__ . '/../responses/' . $file);
    }
}

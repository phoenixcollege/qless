<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 11/4/16
 * Time: 11:38 AM
 */

namespace App\Tests;
use App\Clients\Guzzle;
use App\Providers\HandlerServiceProvider;
use App\Providers\StorageServiceProvider;
use Mockery as m;

class QueuesControllerTest extends TestCase
{

    protected $mock_client;

    public function setUp()
    {
        parent::setUp();
        $this->setupConfig();
        $client_cls = m::mock('GuzzleClient');
        $this->mock_client = $client_cls;
        $client = new Guzzle($client_cls, config('qless', []));
        $this->app['App\Contracts\Client'] = $client;
        $sp = new StorageServiceProvider($this->app);
        $sp->register();
        $hp = new HandlerServiceProvider($this->app);
        $hp->register();
    }

    public function tearDown()
    {
        parent::tearDown();
        m::close();
    }

    public function testNotAuthIs403()
    {
        $this->post('/queues', [], ['HTTP_REFERER' => 'http://localhost']);
        $this->assertResponseStatus(403);
    }

    public function testIs404WithoutIds()
    {
        $this->post('/queues', ['key' => 'abc'], ['HTTP_REFERER' => 'http://localhost']);
        $this->assertResponseStatus(404);
    }

    public function testAuthFailsIs403()
    {
        $this->post('/queues', ['key' => 'def'], ['HTTP_REFERER' => 'http://localhost']);
        $this->assertResponseStatus(403);
    }

    public function testJsonResponse()
    {
        $loc_resp = m::mock('ResponseLocation');
        $q_resp = m::mock('ResponseQueue');
        $wi_resp = m::mock('ResponseWaitInfo');
        $this->mock_client->shouldReceive('request')->once()->with(
            'GET',
            'api/v1/kiosk/locations/1',
            []
        )->andReturn($loc_resp);
        $loc_resp->shouldReceive('getStatusCode')->twice()->andReturn(200);
        $loc_resp->shouldReceive('getBody')->once()->andReturn($this->getXml('locations_1.xml'));
        $this->mock_client->shouldReceive('request')->once()->with(
            'GET',
            'api/v1/kiosk/queues/10',
            []
        )->andReturn($q_resp);
        $q_resp->shouldReceive('getStatusCode')->twice()->andReturn(200);
        $q_resp->shouldReceive('getBody')->once()->andReturn($this->getXml('queues_10.xml'));
        $this->mock_client->shouldReceive('request')->once()->with(
            'GET',
            'api/v1/kiosk/queues/10/waitInfo',
            []
        )->andReturn($wi_resp);
        $wi_resp->shouldReceive('getStatusCode')->twice()->andReturn(200);
        $wi_resp->shouldReceive('getBody')->once()->andReturn($this->getXml('waitInfos_10.xml'));
        $r = $this->call('POST', '/queues', ['key' => 'abc', 'location_ids' => ['1'], 'queue_ids' => ['10']], [], [], ['HTTP_REFERER' => 'http://localhost']);
        $json = '{"status":200,"error":false,"results":[{"id":"1","name":"Tim\'s Pizza","queues":{"10":{"id":"10","location_id":"1","name":"2-person table","state":"open","waitInfo":{"queue_id":"10","wait_time":"25 min","people_in_line":"6"}}}}]}';
        $this->assertEquals($json, $r->getContent());
    }

    public function testHtmlResponse()
    {
        $loc_resp = m::mock('ResponseLocation');
        $q_resp = m::mock('ResponseQueue');
        $wi_resp = m::mock('ResponseWaitInfo');
        $this->mock_client->shouldReceive('request')->once()->with(
            'GET',
            'api/v1/kiosk/locations/1',
            []
        )->andReturn($loc_resp);
        $loc_resp->shouldReceive('getStatusCode')->twice()->andReturn(200);
        $loc_resp->shouldReceive('getBody')->once()->andReturn($this->getXml('locations_1.xml'));
        $this->mock_client->shouldReceive('request')->once()->with(
            'GET',
            'api/v1/kiosk/queues/10',
            []
        )->andReturn($q_resp);
        $q_resp->shouldReceive('getStatusCode')->twice()->andReturn(200);
        $q_resp->shouldReceive('getBody')->once()->andReturn($this->getXml('queues_10.xml'));
        $this->mock_client->shouldReceive('request')->once()->with(
            'GET',
            'api/v1/kiosk/queues/10/waitInfo',
            []
        )->andReturn($wi_resp);
        $wi_resp->shouldReceive('getStatusCode')->twice()->andReturn(200);
        $wi_resp->shouldReceive('getBody')->once()->andReturn($this->getXml('waitInfos_10.xml'));
        $r = $this->call('POST', '/queues/html', ['key' => 'abc', 'location_ids' => ['1'], 'queue_ids' => ['10']], [], [], ['HTTP_REFERER' => 'http://localhost']);
        $cont = $r->getContent();
        $this->assertContains('2-person table', $cont);
        $this->assertNotContains('4-person table', $cont);
    }

    protected function getXml($file)
    {
        return file_get_contents(__DIR__ . '/responses/' . $file);
    }

    protected function setupConfig()
    {
        config(['keys' => ['127.0.0.1' => ['key' => 'abc']]]);
        config(
            [
                'qless' =>
                    [
                        'client_options' => [
                            'base_uri' => 'https://merchant.qless.com/qless',
                            'timeout'  => 2,
                            'cookie'   => true,
                        ],
                        'authentication' => [
                            'uri'         => 'authenticator',
                            'principal'   => 'foo',
                            'credentials' => 'bar',
                        ],
                        'locations'      => [
                            'uri' => 'api/v1/kiosk/locations/%s',
                            'ids' => [
                                1, //Location Name
                                2,
                                3,
                            ],
                        ],
                        'queues'         => [
                            'uri' => 'api/v1/kiosk/queues/%s',
                            'ids' => [
                                10, // queue name
                                11,
                            ],
                        ],
                        'waitinfo'       => [
                            'uri' => 'api/v1/kiosk/queues/%s/waitInfo',
                            'ids' => [
                                10, // queue name
                                11,
                            ],
                        ],
                    ],
            ]
        );
    }
}
